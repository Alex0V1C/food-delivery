package com.example.fooddelivery.Restaurant;

import com.example.fooddelivery.Models.Categories;
import com.example.fooddelivery.Models.Meals;

import java.util.List;

public interface RestaurantView {
    void showLoading();
    void hideLoading();
    void setMeals(List<Meals.Meal> meal);
    void setCategory(List<Categories.Category> category);
    void onErrorLoading(String message);
}
