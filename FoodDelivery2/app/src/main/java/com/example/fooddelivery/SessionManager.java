package com.example.fooddelivery;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;

import java.util.HashMap;

public class SessionManager {
    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;
    WifiChecker wifiChecker = new WifiChecker();

    public static  final String LOGIN = "IS_LOGIN";
    public static  final String PREF_NAME = "LOGIN";
    public static  final String NAME = "NAME";
    public static  final String USERNAME = "USERNAME";
    public static  final String EMAIL = "EMAIL";
    public static  final String ID = "ID";
    private static int userId = 0;

    public SessionManager(Context context)
    {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("LOGIN", PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createSession(int id, String name, String userName, String email)
    {
        editor.putBoolean(LOGIN, true);
        editor.putBoolean(PREF_NAME, true);
        //editor.putInt(ID, id);
        userId = id;
        editor.putString(NAME, name);
        editor.putString(USERNAME, userName);
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public boolean isLoggIn()
    {
        return sharedPreferences.getBoolean(LOGIN, false);
    }

    public void checkLogin()
    {
        if(!this.isLoggIn())
        {
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
            ((MainActivity)context).finish();
        }
        else {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if(!wifiManager.isWifiEnabled())
            {
                wifiChecker.checkWifiState(context);
            }
        }
    }

    public HashMap<String, String> getUserDetail()
    {
        HashMap<String, String> user = new HashMap<>();
        user.put(ID, String.valueOf(userId));
        user.put(NAME, sharedPreferences.getString(NAME, null));
        user.put(USERNAME, sharedPreferences.getString(USERNAME, null));
        user.put(EMAIL, sharedPreferences.getString(EMAIL, null));

        return user;
    }

    public void logout()
    {
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, LoginActivity.class);
        context.startActivity(i);
        ((MainActivity)context).finish();
    }

}

