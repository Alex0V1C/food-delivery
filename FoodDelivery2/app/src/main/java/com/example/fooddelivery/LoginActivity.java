package com.example.fooddelivery;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseNotifier{
    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private ProgressBar loadingProgressBar;
    private static final String URL_LOGIN ="http://192.168.1.5:7099/android_register_login/login.php";
    private final WifiChecker wifiChecker = new WifiChecker();

    SessionManager sessionManager;

    public LoginActivity() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_login);

        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled())
        {
            wifiChecker.checkWifiState(this);
        }

        sessionManager = new SessionManager(this);

        this.usernameEditText = (EditText)this.findViewById(R.id.etUsername);
        this.passwordEditText = (EditText)this.findViewById(R.id.etPassword);
        this.loadingProgressBar = (ProgressBar)this.findViewById(R.id.pbloading);
        TextView registerLink = (TextView) this.findViewById(R.id.tvRegisterLink);
        registerLink.setOnClickListener(v -> {
            Intent it = new Intent( LoginActivity.this, RegisterActivity.class);
            LoginActivity.this.startActivity(it);
        });

        this.loginButton = (Button)this.findViewById(R.id.btnLogin);
        this.loginButton.setOnClickListener(v -> {
            String userName = usernameEditText.getText().toString().trim();
            String password = passwordEditText.getText().toString().trim();

            if(!userName.isEmpty() || !password.isEmpty())
            {
                Login(userName, password);
            }
            else
            {
                usernameEditText.setError("Please insert user name");
                passwordEditText.setError("Please insert password");
            }
        });

    }

    private void Login(final String username, final String password)
    {
        loadingProgressBar.setVisibility(View.VISIBLE);
        loginButton.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        JSONArray jsonArray = jsonObject.getJSONArray("login");
                        if (success.equals("1")){
                            for (int i = 0; i < jsonArray.length(); ++i)
                            {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name").trim();
                                String email = object.getString("email").trim();
                                String id = object.getString("id").trim();

                                Toast.makeText(LoginActivity.this,
                                        "Success Login! Your Name: " +name+ "\nYour Email: " +email, Toast.LENGTH_SHORT).show();
                                loadingProgressBar.setVisibility(View.GONE);

                                sessionManager.createSession( new Integer(id), name, username, email);

                                Intent it = new Intent( LoginActivity.this, MainActivity.class);
                                startActivity(it);
                            }
                        }
                    } catch(JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(LoginActivity.this, "Login Error! " + e.toString(), Toast.LENGTH_SHORT).show();
                        loadingProgressBar.setVisibility(View.GONE);
                        loginButton.setVisibility(View.VISIBLE);
                    }

                },
                error -> {
                    Toast.makeText(LoginActivity.this, "Login Error! " + error.toString(), Toast.LENGTH_SHORT).show();
                    loadingProgressBar.setVisibility(View.GONE);
                    loginButton.setVisibility(View.VISIBLE);
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("userName", username);
                params.put("password", password);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}