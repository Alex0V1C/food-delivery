package com.example.fooddelivery;
import android.app.Application;

public class FoodDeliveryApplication extends Application {
    private String mGlobalVariable="Food Delivery";

    public String getGlobalVariable(){
        return mGlobalVariable;
    }
    public void setGlobalVarValue(String str){
        mGlobalVariable += str+"\n";
    }
    public void SetGlobalClear(){
        mGlobalVariable = "";
    }

}