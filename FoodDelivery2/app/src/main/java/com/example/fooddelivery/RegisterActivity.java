package com.example.fooddelivery;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends BaseNotifier {
    EditText etEmail, etName, etUserName, etPassword, etConfirmPassword;
    Button bRegister;
    ProgressBar pbLoading;
    private static final String URL_Register = "http://192.168.1.5:7099/android_register_login/register.php";
    private WifiChecker wifiChecker = new WifiChecker();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/LatoLight.ttf");
        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "fonts/LatoRegular.ttf");

        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled())
        {
            wifiChecker.checkWifiState(this);
        }

        etEmail = (EditText)findViewById(R.id.etEmail);
        etEmail.setTypeface(custom_font);
        etName = (EditText)findViewById(R.id.etName);
        etName.setTypeface(custom_font);
        etUserName = (EditText)findViewById(R.id.etUsername);
        etUserName.setTypeface(custom_font);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etPassword.setTypeface(custom_font);
        etConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);
        etConfirmPassword.setTypeface(custom_font);
        pbLoading = (ProgressBar) findViewById(R.id.pbloading);

        bRegister = (Button) findViewById(R.id.bRegister);
        bRegister.setTypeface(custom_font1);
        final TextView lin = (TextView) findViewById(R.id.lin);
        lin.setTypeface(custom_font1);

        bRegister.setOnClickListener(v -> Regist());

        lin.setOnClickListener(v -> {
            Intent it = new Intent(RegisterActivity.this, LoginActivity.class);
            RegisterActivity.this.startActivity(it);
        });
    }

    private void Regist()
    {
        pbLoading.setVisibility(View.VISIBLE);
        bRegister.setVisibility(View.GONE);

        final String name = this.etName.getText().toString().trim();
        final String userName = this.etUserName.getText().toString().trim();
        final String email = this.etEmail.getText().toString().trim();
        final String password = this.etPassword.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_Register,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")){
                            Toast.makeText(RegisterActivity.this, "Register Success!", Toast.LENGTH_SHORT).show();
                        }
                        pbLoading.setVisibility(View.GONE);
                        Intent it = new Intent( RegisterActivity.this, LoginActivity.class);
                        startActivity(it);
                    } catch(JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(RegisterActivity.this, "Register Error! " + e.toString(), Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                        bRegister.setVisibility(View.VISIBLE);
                    }
                },
                error -> {
                    Toast.makeText(RegisterActivity.this, "Register Error! " + error.toString(), Toast.LENGTH_SHORT).show();
                    pbLoading.setVisibility(View.GONE);
                    bRegister.setVisibility(View.VISIBLE);
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("userName", userName);
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
