package com.example.fooddelivery;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;

public class Contact extends FragmentActivity implements OnMapReadyCallback, TaskLoadedCallback {
    private ImageView imageViewCar;
    TextView receiver;
    EditText subject, message;
    Button sendbtn;
    FusedLocationProviderClient fusedLocationClient;
    LatLng foodDeliveryPoint = new LatLng(44.304303, 23.816911);
    private Polyline currentPolyline;
    private Marker selectedMarker;

    private GoogleMap mapApi;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        receiver = findViewById(R.id.etReceiver);
        subject = findViewById(R.id.etSubject);
        message = findViewById(R.id.etMessage);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapApi);
        mapFragment.getMapAsync(this);

        imageViewCar = findViewById(R.id.imageViewCar);
        imageViewCar.setOnClickListener(v -> {
            Animation myanimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_to_right);
            imageViewCar.startAnimation(myanimation);
        });

        sendbtn = findViewById(R.id.btnSend);
        sendbtn.setOnClickListener(v -> sendMail());

        Animation myanimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_to_right);
        imageViewCar.startAnimation(myanimation);

        getDeviceLocation();
    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapApi = googleMap;
        mapApi.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "onMapReady: No permission", Toast.LENGTH_SHORT).show();
            return;
        }
        mapApi.setMyLocationEnabled(true);
        mapApi.setOnMapLongClickListener(latLng -> {
            try {
                if (selectedMarker != null) {
                    selectedMarker.remove();
                }
                LatLng selectedLatLng = new LatLng(latLng.latitude, latLng.longitude);
                selectedMarker = mapApi.addMarker(new MarkerOptions().position(selectedLatLng).title("Selected location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                String url = getRequestUrl(selectedLatLng, foodDeliveryPoint, "driving");
                new FetchURL(Contact.this).execute(url, "driving");

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void sendMail()
    {
        final String subjecttxt = subject.getText().toString().trim();
        final String receivertxt = receiver.getText().toString().trim();
        Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + receivertxt));

        it.putExtra(Intent.EXTRA_SUBJECT, subjecttxt);
        it.putExtra(Intent.EXTRA_TEXT, message.getText());

        startActivity(Intent.createChooser(it, "Contact Food Delivery Craiova team"));
    }

    private void getDeviceLocation()
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "getDeviceLocation: No permission", Toast.LENGTH_SHORT).show();
                return;
            }
            Task location = fusedLocationClient.getLastLocation();
            location.addOnCompleteListener(task -> {
                if(task.isSuccessful())
                {
                    Location currentLocation = (Location)task.getResult();

                    String url;
                    mapApi.addMarker(new MarkerOptions().position(foodDeliveryPoint).title("Food Delivery Craiova").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    mapApi.animateCamera(CameraUpdateFactory.newLatLngZoom(foodDeliveryPoint, 15));

                    if(currentLocation != null) {
                        LatLng currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                        if (selectedMarker != null)
                        {
                            selectedMarker.remove();
                        }
                        selectedMarker = mapApi.addMarker(new MarkerOptions().position(currentLatLng).title("Your location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                        url = getRequestUrl(currentLatLng, foodDeliveryPoint, "driving");
                        new FetchURL(Contact.this).execute(url, "driving");
                    }
                }
                else {
                    mapApi.addMarker(new MarkerOptions().position(foodDeliveryPoint).title("Food Delivery Craiova").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    mapApi.moveCamera(CameraUpdateFactory.newLatLng(foodDeliveryPoint));
                    Toast.makeText(Contact.this, "Unable to get device location", Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private String getRequestUrl(LatLng currentLocation, LatLng foodDelivery, String directionType) {
        String origin = "origin=" + currentLocation.latitude + "," + currentLocation.longitude;
        String destination = "destination=" + foodDelivery.latitude + "," + foodDelivery.longitude;
        String sensor = "sensor=false";
        String mode = "mode=" + directionType;

        String param = origin + "&" + destination + "&" + sensor + "&" + mode;

        String output = "json";

        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param
                + "&key=" + getString(R.string.map_key);
    }

    @Override
    public void onTaskDone(Object... values) {
        if(currentPolyline != null)
        {
            currentPolyline.remove();
        }

        currentPolyline = mapApi.addPolyline((PolylineOptions) values[0]);
    }
}
