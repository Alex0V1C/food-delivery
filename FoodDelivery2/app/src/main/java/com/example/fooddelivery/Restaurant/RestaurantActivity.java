package com.example.fooddelivery.Restaurant;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fooddelivery.Api.HelperUtils;
import com.example.fooddelivery.BaseNotifier;
import com.example.fooddelivery.Category.CategoryActivity;
import com.example.fooddelivery.Models.Categories;
import com.example.fooddelivery.Models.Meals;
import com.example.fooddelivery.R;
import com.example.fooddelivery.RecyclerViewMealByCategory;
import com.example.fooddelivery.RecyclerViewRestaurantAdapter;
import com.example.fooddelivery.ViewPagerHeaderAdapter;
import com.google.gson.Gson;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestaurantActivity extends BaseNotifier implements RestaurantView, TextWatcher {

    private static final Map<String, Meals.Meal> allMeals = new HashMap<>();
    private static List<Categories.Category> allCategories = new ArrayList<>();
    private static boolean searchWasPerformed = false;
    private final List<String> mealNames = new ArrayList<>();
    private Dialog cartDialog;

    public static Map<String, Integer> mealPrices = new HashMap<>();
    public static Map<String, Integer> mealQuantities = new HashMap<>();

    public static final String EXTRA_CATEGORY = "category";
    public static final String EXTRA_POSITION = "position";

    @BindView(R.id.viewPageHeader) ViewPager viewpagerMeal;
    @BindView(R.id.RestaurantRecyclerView) RecyclerView recyclerViewCategory;
    @BindView(R.id.searchText) AutoCompleteTextView searchTextView;
    @BindView(R.id.searchButton) ImageButton searchButton;
    @BindView(R.id.titleRestaurant) TextView titleGrid;

    static ImageView shoppingCart;
    RestaurantPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurants_activity);
        ButterKnife.bind(this);

        shoppingCart = (ImageView) findViewById(R.id.shoppingCart);
        presenter = new RestaurantPresenter(this);
        presenter.getMeals();

        presenter.getCategories();

        searchTextView.addTextChangedListener(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mealNames);

        searchTextView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        searchButton.setOnClickListener(v -> {
            try {
                SearchCategory();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        cartDialog = new Dialog(this);
        cartDialog.setContentView(R.layout.shopping_cart_popup);
        shoppingCart.setOnClickListener(this::ShowCartPopup);

        File folder = new File( this.getFilesDir() + "/Lucene Resources");
        if (!folder.exists()) {
            folder.mkdir();
        }
    }

    @Override
    public void showLoading() {
        //findViewById(R.id.shimmer_view_container2).setVisibility(View.VISIBLE);
        findViewById(R.id.shimmerCategory).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        //findViewById(R.id.shimmer_view_container2).setVisibility(View.GONE);
        findViewById(R.id.shimmerCategory).setVisibility(View.GONE);
    }

    @Override
    public void setMeals(List<Meals.Meal> meal) {
        for (Meals.Meal mealResult : meal)
        {
            Log.w("meal name : ", mealResult.getStrMeal());
        }
        ViewPagerHeaderAdapter headerAdapter = new ViewPagerHeaderAdapter(meal, this);
        viewpagerMeal.setAdapter(headerAdapter);
        viewpagerMeal.setPadding(20, 0 ,150, 0);
        headerAdapter.notifyDataSetChanged();

        headerAdapter.setOnItemClickListener((v, position) -> Toast.makeText(this, meal.get(position).getStrMeal(), Toast.LENGTH_SHORT).show());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void setCategory(List<Categories.Category> category) {
        RecyclerViewRestaurantAdapter restaurantAdapter = new RecyclerViewRestaurantAdapter(category, this);
        recyclerViewCategory.setAdapter(restaurantAdapter);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3,
                GridLayoutManager.VERTICAL, false);
        recyclerViewCategory.setLayoutManager(layoutManager);
        recyclerViewCategory.setNestedScrollingEnabled(true);
        restaurantAdapter.notifyDataSetChanged();

        restaurantAdapter.setOnItemClickListener((view, position) -> {
            Toast.makeText(this, category.get(position).getStrCategory(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, CategoryActivity.class);
            intent.putExtra(EXTRA_CATEGORY, (Serializable) category);
            intent.putExtra(EXTRA_POSITION, position);
            startActivity(intent);
        });

        if (allCategories == null || allCategories.size() == 0) {
            allCategories = category;
            GetAllMeals();
        }

        titleGrid.setText("Meal Categories");

        ChangeCartIcon(this);
    }

    @Override
    public void onErrorLoading(String message) {
        HelperUtils.showDialogMessage(this, "Title", message);
    }

    private void GetAllMeals()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        Gson gson = new Gson();

        for (Categories.Category c: allCategories)
        {
            String url ="https://www.themealdb.com/api/json/v2/1/filter.php?c=" + c.getStrCategory();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response -> {
                        Toast.makeText(RestaurantActivity.this,
                                "Load meals for " + c.getStrCategory() + " category", Toast.LENGTH_SHORT).show();
                        Meals responseMeals = (Meals)gson.fromJson(response, Meals.class);
                        List<Meals.Meal> newMeals = responseMeals.getMeals();
                        for (Meals.Meal nm: newMeals)
                        {
                            allMeals.put(nm.getIdMeal(), nm);
                        }

                        Random random = new Random();
                        for (Meals.Meal m : newMeals)
                        {
                            String name = m.getStrMeal();
                            mealNames.add(name);
                            Integer randomNumber = random.nextInt(36 - 10) + 10;
                            mealPrices.put(m.getIdMeal(), randomNumber);
                            mealQuantities.put(m.getIdMeal(), 0);
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(RestaurantActivity.this,
                                android.R.layout.simple_list_item_1, mealNames);
                        searchTextView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }, error -> Toast.makeText(RestaurantActivity.this, "That didn't work!", Toast.LENGTH_SHORT).show());

            queue.add(stringRequest);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void SearchCategory()
    {
        final String text = searchTextView.getText().toString().trim();
        if (text.trim().equalsIgnoreCase("") && text.length() >= 3) {
            if (allMeals != null) {
                Toast.makeText(this, "Meals for: " + text, Toast.LENGTH_SHORT).show();

                List<Meals.Meal> result = doSearch(text);
                RecyclerViewMealByCategory restaurantAdapter = new RecyclerViewMealByCategory(this, result);
                recyclerViewCategory.setAdapter(restaurantAdapter);
                GridLayoutManager layoutManager = new GridLayoutManager(this, 3,
                        GridLayoutManager.VERTICAL, false);
                recyclerViewCategory.setLayoutManager(layoutManager);
                recyclerViewCategory.setNestedScrollingEnabled(true);
                restaurantAdapter.notifyDataSetChanged();
                searchWasPerformed = true;
                titleGrid.setText("Meals for: " + text);
            }
        } else if (searchWasPerformed) {
            searchWasPerformed = false;
            setCategory(allCategories);
            titleGrid.setText("Meal Categories");
        }

        ChangeCartIcon(this);
    }

    private List<Meals.Meal>  doSearch(String text)
    {
        List<Meals.Meal> result = new ArrayList<>();
       for (Meals.Meal m : allMeals.values())
       {
           String name = m.getStrMeal().toLowerCase();
           if (name.contains(text.toLowerCase()))
           {
               result.add(m);
           }
       }
       return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void ChangeCartIcon(Context context)
    {
        boolean isCartEmpty = true;
        for (Integer q :RestaurantActivity.mealQuantities.values()) {
            if(q > 0)
            {
                isCartEmpty = false;
                break;
            }
        }

        if(isCartEmpty)
        {
            shoppingCart.setImageIcon(Icon.createWithResource(context, R.drawable.icon_shopping_cart_empty));
        }
        else
        {
            shoppingCart.setImageIcon(Icon.createWithResource(context, R.drawable.icon_shopping_cart));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void ShowCartPopup(View v)
    {
        TextView txtClose, totalPrice;
        Button btnOrder;
        ListView listview;

        txtClose = (TextView)cartDialog.findViewById(R.id.closeBtn);
        listview = (ListView)cartDialog.findViewById(R.id.listview);
        totalPrice = (TextView)cartDialog.findViewById(R.id.totalPrice);
        btnOrder = (Button) cartDialog.findViewById(R.id.btnOrder);
        btnOrder.setVisibility(View.VISIBLE);

        ArrayList<String> selectedMeals = new ArrayList<>();

        int totalPriceSum = 0;
        for (Map.Entry<String, Integer> mealQuantityEntry : mealQuantities.entrySet()) {
            String key = mealQuantityEntry.getKey();
            Integer value = mealQuantityEntry.getValue();

            if (value > 0)
            {
                int pricePerMeal = value * mealPrices.get(key);
                totalPriceSum +=  pricePerMeal;
                for(Map.Entry<String, Meals.Meal> entry : allMeals.entrySet()) {
                    Meals.Meal valueM = entry.getValue();
                    if(valueM.getIdMeal().trim().equalsIgnoreCase(key)) {
                        selectedMeals.add(valueM.getStrMeal() + "    x" + value + "  :   " + pricePerMeal + "$");
                    }
                }
            }
        }
        totalPrice.setText("Total price: " + totalPriceSum + "$");

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, selectedMeals);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        listview.setOnItemClickListener((adapterView, view, which, l) -> Log.d("Shopping cart", "meals: " + selectedMeals));

        btnOrder.setOnClickListener(v12 -> {
            totalPrice.setText("Total price: " + 0);
            mealQuantities.replaceAll((key, oldValue) -> 0);
            listview.setAdapter(null);
            btnOrder.setVisibility(View.GONE);
        });

        txtClose.setOnClickListener(v1 -> {
            ChangeCartIcon(v1.getContext());
            cartDialog.dismiss();
        });

        cartDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cartDialog.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
