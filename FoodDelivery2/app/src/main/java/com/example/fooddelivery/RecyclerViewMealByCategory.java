package com.example.fooddelivery;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fooddelivery.Models.Meals;
import com.example.fooddelivery.Restaurant.RestaurantActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewMealByCategory extends RecyclerView.Adapter<RecyclerViewMealByCategory.RecyclerViewHolder> {

    private List<Meals.Meal> meals;
    private Context context;
    private static ClickListener clickListener;

    public RecyclerViewMealByCategory(Context context, List<Meals.Meal> meals) {
        this.meals = meals;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerViewMealByCategory.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_meal,
                viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewMealByCategory.RecyclerViewHolder viewHolder, int i) {

        String strMealThumb = meals.get(i).getStrMealThumb();
        Picasso.get().load(strMealThumb).placeholder(R.drawable.shadow_bottom_to_top).into(viewHolder.mealThumb);

        String strMealName = meals.get(i).getStrMeal();
        viewHolder.mealName.setText(strMealName);

        Integer price = RestaurantActivity.mealPrices.get( meals.get(i).getIdMeal());
        viewHolder.mealPrice.setText(price + "$");

        Integer quantity = RestaurantActivity.mealQuantities.get(meals.get(i).getIdMeal());
        viewHolder.mealQuantity.setText(String.valueOf(quantity));
        if (quantity == 0)
        {
            viewHolder.mealQuantity.setVisibility(View.GONE);
        }

        viewHolder.buttonPlus.setOnClickListener(new View.OnClickListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(View v) {
                ModifyQuantity(viewHolder, i, meals.get(i).getIdMeal(), true);
            }
        });

        viewHolder.buttonMinus.setOnClickListener(new View.OnClickListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(View v) {
                ModifyQuantity(viewHolder, i, meals.get(i).getIdMeal(), false);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void ModifyQuantity(RecyclerViewHolder viewHolder, int index, String mealId, Boolean add) {
        Integer quantity = RestaurantActivity.mealQuantities.get(meals.get(index).getIdMeal());

        if (add)
        {
            quantity += 1;
            viewHolder.mealQuantity.setText(String.valueOf(quantity));
        }
        else
        {
            if (quantity != 0) {
                quantity -= 1;
                viewHolder.mealQuantity.setText(String.valueOf(quantity));
            }
        }

        if (quantity > 0)
            viewHolder.mealQuantity.setVisibility(View.VISIBLE);
        else
            viewHolder.mealQuantity.setVisibility(View.GONE);

        RestaurantActivity.mealQuantities.remove(meals.get(index).getIdMeal());
        RestaurantActivity.mealQuantities.put(meals.get(index).getIdMeal(), quantity);

        RestaurantActivity.ChangeCartIcon(context);
    }

    @Override
    public int getItemCount() {
        return meals.size();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.mealThumb) ImageView mealThumb;
        @BindView(R.id.mealName) TextView mealName;
        @BindView(R.id.mealPrice) TextView mealPrice;
        @BindView(R.id.mealQuantity) TextView mealQuantity;

        @BindView(R.id.plus) ImageView buttonPlus;
        @BindView(R.id.minus) ImageView buttonMinus;

        RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition());
        }
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        RecyclerViewMealByCategory.clickListener = clickListener;
    }


    public interface ClickListener {
        void onClick(View view, int position);
    }
}

