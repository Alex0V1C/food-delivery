package com.example.fooddelivery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class BaseNotifier extends AppCompatActivity implements SensorEventListener {
    private static float lastX = 0;
    private static float lastY = 0;
    private static float lastZ = 0;
    private static final float shakeThreadHold = 5f;
    private static boolean itIsNotFirstTime = false, isAccelerometerAvailable = false;
    private static SensorManager sensorManager;
    private static Sensor accSensor;
    private static Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            Log.d("Orientation", "ORIENTATION_PORTRAIT");
        }
        else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            Log.d("Orientation", "ORIENTATION_LANDSCAPE");
        }

        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
        {
            accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            isAccelerometerAvailable = true;
        }
        else
        {
            Toast.makeText(this, "ACCELEROMETER sensor is not available", Toast.LENGTH_SHORT).show();
            isAccelerometerAvailable = false;
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(wifiStateReceiver, intentFilter);
    }

    private final BroadcastReceiver wifiStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int wifiStateExtra = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                    WifiManager.WIFI_STATE_UNKNOWN);
            switch (wifiStateExtra)
            {
                case WifiManager.WIFI_STATE_ENABLED:
                    Log.d("Wifi", "Wifi is ON");
                        WifiChecker.closePopup();
                    break;
                case WifiManager.WIFI_STATE_DISABLED:
                    WifiChecker.checkWifiState(context);
                    break;
            }
        }
    };

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        if(event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
        {
            return;
        }

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        double angle = (Math.atan2(y, Math.sqrt(x*x+z*z)) / (Math.PI /180));
        Log.d("Accelerometer", "Accelerometer angle = " + angle);

        if(itIsNotFirstTime)
        {
            float xdif = Math.abs(lastX - x);
            float ydif = Math.abs(lastY - y);
            float zdif = Math.abs(lastZ - z);

            if((xdif > shakeThreadHold && ydif > shakeThreadHold)
                    || (ydif > shakeThreadHold && zdif > shakeThreadHold)
                    || (xdif > shakeThreadHold && zdif > shakeThreadHold) )
            {
                vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE));
            }
        }

        lastX = x;
        lastY = y;
        lastZ = z;
        itIsNotFirstTime = true;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isAccelerometerAvailable)
        {
            sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isAccelerometerAvailable)
        {
            sensorManager.unregisterListener(this);
        }
    }
}
