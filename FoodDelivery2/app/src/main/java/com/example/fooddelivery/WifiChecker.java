package com.example.fooddelivery;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Icon;
import android.net.wifi.WifiManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class WifiChecker {
    static Dialog wifiDialog;

    static public void checkWifiState(Context context)
    {
        wifiDialog = new Dialog(context);
        wifiDialog.setContentView(R.layout.wifi_popup);

        TextView txtInformation;
        Button wifiBtn;
        ImageView wifiImage;

        txtInformation = (TextView) wifiDialog.findViewById(R.id.wifiText);
        wifiImage = (ImageView) wifiDialog.findViewById(R.id.wifiImage);
        wifiBtn = (Button) wifiDialog.findViewById(R.id.wifiBtn);

        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        boolean state = wifiManager.isWifiEnabled();

        wifiBtn.setOnClickListener(v -> {
            boolean state1 = wifiManager.setWifiEnabled(true);
            if(state1)
            {
                wifiImage.setImageIcon(Icon.createWithResource(context, R.drawable.wifi_on_blue));
                txtInformation.setText("Wifi is ON! Close the pop up");
                Toast.makeText(context, "Wifi is ON!", Toast.LENGTH_SHORT).show();
                wifiDialog.dismiss();
            }
            else
            {
                txtInformation.setText("No internet connection found. Try again!");
            }
        });

        wifiDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(!state)
        {
            wifiDialog.show();
        }
    }

    static public void closePopup()
    {
        if(wifiDialog != null && wifiDialog.isShowing())
        {
            wifiDialog.dismiss();
        }
    }
}
