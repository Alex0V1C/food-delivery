package com.example.fooddelivery;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseNotifier {
    private static String localPathToRegisterPHP = "http://192.168.1.5:7099/android_register_login/";

    private static String readDataURL = localPathToRegisterPHP + "read_detail.php";
    private static String editDataURL = localPathToRegisterPHP +  "edit_detail.php";
    private static String uploadDataURL = localPathToRegisterPHP + "upload.php";
    private static String deletePhotoURL = localPathToRegisterPHP + "delete_photo.php";

    final String successMessage = "Success!";
    final String errorTryAgainMessage = "Error! Try Again! ";

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = ProfileActivity.class.getSimpleName();
    EditText realName, email;
    Button saveBtn, editImageBtn, openCameraBtn, deleteImageBtn;
    CircleImageView profileImage;
    static String intentId;
    private Menu actionMenu;
    private String userName;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        realName = (EditText) findViewById(R.id.realNameProfile);
        email = (EditText)findViewById(R.id.emailprofile);
        saveBtn = (Button)findViewById(R.id.saveUserDaataBtn);
        saveBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                executeSave();
            }
        });

        editImageBtn = (Button)findViewById(R.id.editImageBtn);

        editImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseFile();
            }
        });
        profileImage = (CircleImageView)findViewById(R.id.profileImage);

        openCameraBtn = (Button)findViewById(R.id.takeImageBtn);
        openCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureWithCamera();
            }
        });

        deleteImageBtn = (Button)findViewById(R.id.deleteImageBtn);
        deleteImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePicture();
            }
        });

        Intent intent = getIntent();
        intentId = intent.getStringExtra("id");

        sessionManager = new SessionManager(this);
    }

    private void getUserDetail()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, readDataURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            String success = obj.getString("success");
                            JSONArray jsonArray = obj.getJSONArray("read");

                            if(success.equals("1"))
                            {
                                for(int i = 0; i < jsonArray.length(); ++i)
                                {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String strName = object.getString("name").trim();
                                    String strUserName = object.getString("userName").trim();
                                    String strEmail = object.getString("email").trim();
                                    String strPhoto = object.getString("photo").trim();

                                    byte[] bytes = strPhoto.getBytes(StandardCharsets.UTF_8);
                                    byte[]  decodedPhoto = Base64.decode(strPhoto, Base64.DEFAULT);

                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                                    if (strPhoto != null && profileImage != null) {
                                        profileImage.setImageBitmap(bitmap);
                                        Drawable d = Drawable.createFromStream(new ByteArrayInputStream(decodedPhoto), "profile_image");
                                        profileImage.setImageDrawable(d);
                                    }
                                    realName.setText(strName);
                                    email.setText(strEmail);
                                    userName = strUserName;
                                }
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Toast.makeText(ProfileActivity.this, "Error Reading Detail" + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this, "Error Reading Detail" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                String id = intentId;
                params.put("id", id);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        getUserDetail();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_action, menu);
        actionMenu = menu;
        actionMenu.findItem(R.id.menu_save).setVisible(false);

        return true;
    }

    private void executeSave() {
        StringRequest stringRequest= new StringRequest(Request.Method.POST, editDataURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");

                            if(success.equals("1"))
                            {
                                Toast.makeText(ProfileActivity.this, successMessage, Toast.LENGTH_SHORT).show();
                                sessionManager.createSession( new Integer(intentId), realName.getText().toString(), userName, email.getText().toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ProfileActivity.this, "Error " + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        error.printStackTrace();
                        Toast.makeText(ProfileActivity.this, "Error " + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected  Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("name", realName.getText().toString());
                params.put("userName", userName);
                params.put("email", email.getText().toString());
                params.put("id", intentId);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void takePictureWithCamera()
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void chooseFile()
    {
        Intent it = new Intent();
        it.setType("image/*");
        it.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(it, "Select Picture"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentData) {
        super.onActivityResult(requestCode, resultCode, intentData);

        if(requestCode == 1 && resultCode == RESULT_OK && intentData != null)
        {
            if (intentData.getData() != null) {
                Uri filePath = intentData.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    profileImage.setImageBitmap(bitmap);
                    uploadPicture(getStringImage(bitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Bundle extras = intentData.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                profileImage.setImageBitmap(imageBitmap);
                uploadPicture(getStringImage(imageBitmap));
            }
        }
    }

    private String getStringImage(Bitmap image)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 90, stream);

        byte[] imageArray = stream.toByteArray();
        return Base64.encodeToString(imageArray, Base64.DEFAULT);
    }

    private void uploadPicture(final String photo)
    {
        StringRequest request = new StringRequest(Request.Method.POST, uploadDataURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResult = new JSONObject(response);
                            String success = jsonResult.getString("success");

                            if(success.equals("1"))
                            {
                                Toast.makeText(ProfileActivity.this, successMessage, Toast.LENGTH_SHORT);
                                getUserDetail();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ProfileActivity.this, errorTryAgainMessage + e.toString(), Toast.LENGTH_SHORT);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this, errorTryAgainMessage  + error.toString(), Toast.LENGTH_SHORT);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("id", intentId);
                params.put("photo", photo);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    private void deletePicture()
    {
        StringRequest request = new StringRequest(Request.Method.POST, deletePhotoURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jobj = new JSONObject(response);
                            String success = jobj.getString("success");

                            if(success.equals("1"))
                            {
                                Toast.makeText(ProfileActivity.this, successMessage, Toast.LENGTH_SHORT);
                                getUserDetail();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ProfileActivity.this, errorTryAgainMessage + e.toString(), Toast.LENGTH_SHORT);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this, errorTryAgainMessage  + error.toString(), Toast.LENGTH_SHORT);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("id", intentId);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }
}
