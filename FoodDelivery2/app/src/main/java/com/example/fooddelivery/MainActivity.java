package com.example.fooddelivery;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fooddelivery.Restaurant.RestaurantActivity;

import java.util.HashMap;

public class MainActivity extends BaseNotifier {
    TextView etEmail, etName, etUserName, contactLink;
    Button bLogout, bRestaurant;
    ImageView btnProfile;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager = new SessionManager(this);
        sessionManager.checkLogin();

        HashMap<String, String> user = sessionManager.getUserDetail();
        String name = user.get(sessionManager.NAME);
        String userName = user.get(sessionManager.USERNAME);
        String email = user.get(sessionManager.EMAIL);
        String userId = user.get(sessionManager.ID);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/LatoLight.ttf");
        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "fonts/LatoRegular.ttf");


        etEmail = (TextView)findViewById(R.id.etEmail);
        etEmail.setTypeface(custom_font);
        etEmail.setText(email);

        etName = (TextView)findViewById(R.id.etName);
        etName.setTypeface(custom_font);
        etName.setText(name);

        etUserName = (TextView)findViewById(R.id.etUsername);
        etUserName.setTypeface(custom_font);
        etUserName.setText(userName);

        bRestaurant = (Button) findViewById(R.id.bRestaurant);
        bRestaurant.setTypeface(custom_font1);
        this.bRestaurant.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent( MainActivity.this, RestaurantActivity.class);
                MainActivity.this.startActivity(it);
            }
        });

        bLogout = (Button) findViewById(R.id.bLogout);
        bLogout.setTypeface(custom_font1);
        this.bLogout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                sessionManager.logout();
            }
        });

        this.contactLink = (TextView)this.findViewById(R.id.tvContactLink);
        this.contactLink.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent( MainActivity.this, Contact.class);
                startActivity(it);
            }
        });

        this.btnProfile = (ImageView) findViewById(R.id.editProfileBtn);
        btnProfile.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent( MainActivity.this, ProfileActivity.class);
                it.putExtra("id", userId);
                it.putExtra("email", etEmail.getText());
                it.putExtra("name", etEmail.getText());
                it.putExtra("username", etEmail.getText());
                startActivity(it);
            }
        });
    }


}
