package com.example.fooddelivery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fooddelivery.Models.Meals;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YoutubeTutorial extends YouTubeBaseActivity {
    YouTubePlayerView youtubePlayerView;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    Button brnStart;
    TextView textinstructions, closeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_tutorial);

        brnStart = (Button) findViewById(R.id.btnStartYoutube);
        textinstructions = (TextView) findViewById(R.id.tvinstructions);
        closeBtn = (TextView) findViewById(R.id.closeBtn);

        Intent intent = getIntent();
        //String link = intent.getStringExtra("link");
        String mealName = intent.getStringExtra("mealName");

        getInstructionsAndLink(mealName);

        youtubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubeView);

        brnStart.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                youtubePlayerView.initialize("AIzaSyDMDDJ-2Td44dXZBraRev3JrnePpljNpno", onInitializedListener);
            }
        });

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent it = new Intent( YoutubeTutorial.this, CategoryFragment.class);
                //startActivity(it);
                YoutubeTutorial.this.finish();
            }
        });
    }

    private void getInstructionsAndLink(String mealName)
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        Gson gson = new Gson();

        String url = "https://www.themealdb.com/api/json/v2/1/search.php?s=" + mealName;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Meals responseMeals = (Meals) gson.fromJson(response, Meals.class);

                        List<Meals.Meal> newMeals = responseMeals.getMeals();
                        textinstructions.setText(newMeals.get(0).getStrInstructions());

                        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
                            @Override
                            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                                String link = newMeals.get(0).getStrYoutube();
                                if (link == null || link.isEmpty())
                                {
                                    link = "RQlp-p_Qcsw";
                                }
                                else
                                {
                                    String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

                                    Pattern compiledPattern = Pattern.compile(pattern);
                                    Matcher matcher = compiledPattern.matcher(link);

                                    if(matcher.find()){
                                        link =  matcher.group();
                                    }
                                }
                                youTubePlayer.loadVideo(link);
                            }

                            @Override
                            public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                                YouTubeInitializationResult youTubeInitializationResult) {
                                Toast.makeText(getApplicationContext(), "youtube player error", Toast.LENGTH_SHORT).show();
                            }
                        };
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(YoutubeTutorial.this, "That didn't work!", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }
}
