package com.example.fooddelivery.Restaurant;

import androidx.annotation.NonNull;

import com.example.fooddelivery.Api.HelperUtils;
import com.example.fooddelivery.Models.Categories;
import com.example.fooddelivery.Models.Meals;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantPresenter {
    private final RestaurantView view;

    public RestaurantPresenter(RestaurantView view) {
        this.view = view;
    }

    void getMeals() {
            view.showLoading();
            Call<Meals> mealsCall = HelperUtils.GetFoodApi().getMeal();

            mealsCall.enqueue(new Callback<Meals>() {
                @Override
                public void onResponse(@NonNull Call<Meals> call, @NonNull Response<Meals> response) {
                    view.hideLoading(); //Close loading when you have received a response from the server

                    // Non-empty results check
                    if (response.isSuccessful() && response.body() != null) {
                       view.setMeals(response.body().getMeals());
                    }
                    else {
                        view.onErrorLoading(response.message());
                    }
                }

                @Override
                public void onFailure(Call<Meals> call, Throwable t) {
                    //  Close loading
                    view.hideLoading();
                    // Show an error message
                    view.onErrorLoading(t.getLocalizedMessage());
                }
            });
    }


    void getCategories() {
            view.showLoading();

            Call<Categories> categoriesCall = HelperUtils.GetFoodApi().getCategories();
            categoriesCall.enqueue(new Callback<Categories>() {
                @Override
                public void onResponse(@NonNull Call<Categories> call,
                                       @NonNull  Response<Categories> response) {
                    view.hideLoading();
                    if (response.isSuccessful() && response.body() != null) {
                        view.setCategory(response.body().getCategories());
                    }
                    else {
                        view.onErrorLoading(response.message());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Categories> call, @NonNull Throwable t) {
                    //  Close loading
                    view.hideLoading();
                    // Show an error message
                    view.onErrorLoading(t.getLocalizedMessage());
                }
            });
        }
    }

