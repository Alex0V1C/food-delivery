package com.example.fooddelivery;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
